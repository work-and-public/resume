# Curriculum Vitae (Resume)

![Resume](https://gitlab.com/work-and-public/cv/-/raw/master/generated/resume.png)

## Icons and Images

For copyright purposes, all the `.svg` icons were downloaded from [svgrepo.com](https://www.svgrepo.com/) and only used if their licenses would allow its usage.

This warning serves the purpose of giving credit to the creators of the vector images used.

## License

Copyright © 2022 Rafael Moreira

This repository and all it's code is licensed under the [MIT License](./LICENSE).
